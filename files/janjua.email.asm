# Author: Weqaar Janjua
# GNU Assembler src

# as --64 -o janjua.email.o janjua.email.asm
# ld janjua.email.o -o janjua.email.bin
# ./janjua.email.bin

.intel_syntax noprefix

.section .data
    first:
        .ascii  "\x77\x65\x71\x61\x61\x72\x2e"
    last:
        .ascii  "\x6a\x61\x6e\x6a\x75\x61\x40"
    tld:
        .ascii  "\x67\x6d\x61\x69\x6c\x2e\x63\x6f\x6d"
    crlf:
        .ascii  "\xD\xA"

.section .text

    .global  _start

    _start:
        mov     rax, 1                          # system call 1 is write()
        mov     rdi, 1                          # file handle 1 is stdout
        mov     rsi, OFFSET FLAT:first          # address of string to output
        mov     rdx, 7                          # number of bytes
        syscall                                 # invoke operating system to do the write
        mov     rax, 1                          # system call 1 is write()
        mov     rdi, 1                          # file handle 1 is stdout
        mov     rsi, OFFSET FLAT:last           # address of string to output
        mov     rdx, 7                          # number of bytes
        syscall                                 # invoke operating system to do the write
        mov     rax, 1                          # system call 1 is write()
        mov     rdi, 1                          # file handle 1 is stdout
        mov     rsi, OFFSET FLAT:tld            # address of string to output
        mov     rdx, 9                          # number of bytes
        syscall                                 # invoke operating system to do the write
        mov     rax, 1                          # system call 1 is write()
        mov     rdi, 1                          # file handle 1 is stdout
        mov     rsi, OFFSET FLAT:crlf           # address of string to output
        mov     rdx, 2                          # number of bytes
        syscall                                 # invoke operating system to do the write

        mov     rax, 60                         # system call 60 is exit()
        xor     rdi, rdi                        # exit code 0
        syscall                                 # invoke operating system to exit

# EOF, please do not SPAM

