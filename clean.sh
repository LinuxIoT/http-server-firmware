#!/bin/bash

echo "Cleaning up..."
find . -name '*.pyc' | xargs rm 2>/dev/null
find . -name '*.out.txt' | xargs rm 2>/dev/null
find . -name '*.py.*.txt' | xargs rm 2>/dev/null
find . -name '*.debug.log' | xargs rm 2>/dev/null
find . -name '*.error.log' | xargs rm 2>/dev/null
find . -name '*.stats.log' | xargs rm 2>/dev/null
find . -name '*.pid' | xargs rm 2>/dev/null
echo "Done."
