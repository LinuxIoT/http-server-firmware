\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}1. General Introduction}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}2. Product}{1}{section.1.2}
\contentsline {chapter}{\numberline {2}Product Brief}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}1. Product}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}2. Hardware}{4}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}2.1. End-point Board}{4}{subsection.2.2.1}
\contentsline {subsubsection}{2.1.1. Block Diagram}{4}{subsubsection*.3}
\contentsline {subsubsection}{2.1.2. Product Specifications}{5}{subsubsection*.4}
\contentsline {subsection}{\numberline {2.2.2}2.2. Program and Debug Board}{7}{subsection.2.2.2}
\contentsline {subsubsection}{2.2.1. Product Specifications}{8}{subsubsection*.5}
\contentsline {subsection}{\numberline {2.2.3}2.3. Heart Rate Pulse Oximeter Board}{8}{subsection.2.2.3}
\contentsline {subsubsection}{2.3.1. Product Specifications}{8}{subsubsection*.6}
\contentsline {subsection}{\numberline {2.2.4}2.4. IoT Gateway Board - C.H.I.P Breakout}{8}{subsection.2.2.4}
\contentsline {subsubsection}{2.4.1. Product Specifications}{9}{subsubsection*.7}
\contentsline {subsection}{\numberline {2.2.5}2.5. Navigation Board}{9}{subsection.2.2.5}
\contentsline {subsubsection}{2.5.1. Product Specifications}{10}{subsubsection*.8}
\contentsline {subsection}{\numberline {2.2.6}2.6. Optical and Thermal Vision Sensor}{11}{subsection.2.2.6}
\contentsline {subsubsection}{2.6.1. Product Specifications}{11}{subsubsection*.9}
\contentsline {subsection}{\numberline {2.2.7}2.7. Board-to-board Flex Connector}{11}{subsection.2.2.7}
\contentsline {subsubsection}{2.7.1. Product Specifications}{12}{subsubsection*.10}
\contentsline {chapter}{\numberline {3}Support}{13}{chapter.3}
