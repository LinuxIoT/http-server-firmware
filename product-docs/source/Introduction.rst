.. _Introduction:

============
Introduction
============


1. General Introduction
=======================

LinuxIoT Project was started by `two individuals <http://www.linuxiot.ie/html/about.html>`_. It is now opensource under GPL.
We work on the project as time permits. With a full-time job, that doesn't leave us with much.
The project is to build a complete IoT Product - Hardware, Software, Webservices, and the necessary Interfaces. 

Internet of Things (IoT) is relatively a newer term in the industry. A decade back, the term "IoT" did not exist, although the technnology did in its crude form. There were ultra-low power sensors, gateways that could communicate with both the sensor networks (over IEEE 802.15.4 and/or proprietary protocols) and the Inter-networks (over 802.11 variants/802.3 variants/IP); the dashboards were there too. Militaries around the globe used such systems i.e. Unattended Ground Sensors (UGS) and Early Warning Systems (PEWS). Many commercial entities used such systems for physical security. This technology later saw its way to the public, similar to the fate of every Military technology.

Thanks to Richard Stallman and Eric S. Raymond, FOSS revolution took IoT as well. Industry invested tremendous amounts of time and money in it, there were forecasts for billions of IoT devices in the years ahead.

This contributed to tremendous growth in the microelectronics (ultra-low power) and the software industry. 

Variety of communication protocols were developed not limited to COAP, MQTT, Thread, and ZigBee.

A lot of knowledge material on IoT seems to be about Data and Analytics. Fair share hasn't been given to the core technology that is Embedded Hardware, Analog Electronics, Operating Systems, Compilers, Middleware, Communication Protocols, and indeed making sense of all the sensor data (most talked about).

Check out `ARM IoT <http://www.arm.com/markets/internet-of-things>`_ site.


2. Product
==========

The Product include necessary Hardware and Software for a fully functional IoT Infrastructure.

* Hardware
  
  #. End-point Board (IoT EP Board as we call it)
  #. Debug & Program Board
  #. Gateway Breakout Board (designed for `C.H.I.P <https://getchip.com/pages/chip>`_ Linux computer)
  #. Heart Rate Pulse Oximeter Sensor - Add-on
  #. Navigation Unit - Add-on
  #. Optical and Thermal Vision Sensor - Add-on
  #. 40-pin Flex Board-to-board Connector


* Firmware

  #. Firmware for IoT End-point board
  #. Firmware for IoT Router & Forwarder (EP Board also functions as dedicated Router/Repeater)
  #. Firmware & Middleware for IoT Gateway (Linux only)


* Applications
  
  #. Webservices Stack
  #. Minimal Dashboard Stack (Web UI)
  #. Mobile Apps for iOS and Android |mob|

  .. |mob| image:: images/icons/ios-android.png
           :width: 15 %
           :scale: 50 %


These boards are WIP (work in progress):

* Add-on Boards

  #. Seismo-acoustic sensor with on-board Signal Processing Unit - Add-on

In the next sections, we explain the Hardware and Software components.

