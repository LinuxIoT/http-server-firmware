.. _Product:

=============
Product Brief
=============


1. Product 
==========

As mentioned in the previous section of this document, the Product include necessary Hardware and Software for a fully functional IoT Infrastructure. This include:


* Hardware
  
  #. End-point Board (IoT EP Board as we call it)
  #. Debug & Program Board
  #. Gateway Breakout Board (designed for `C.H.I.P <https://getchip.com/pages/chip>`_ Linux computer)
  #. Heart Rate Pulse Oximeter Sensor - Add-on
  #. Navigation Unit - Add-on
  #. Optical and Thermal Vision Sensor - Add-on
  #. 40-pin Flex Board-to-board Connector


* Firmware

  #. Firmware for IoT End-point board
  #. Firmware for IoT Router & Forwarder (EP Board also functions as dedicated Router/Repeater)
  #. Firmware & Middleware for IoT Gateway (Linux only)


* Applications
  
  #. Webservices Stack
  #. Minimal Dashboard Stack (Web UI)
  #. Mobile Apps for iOS and Android |mob|

  .. |mob| image:: images/icons/ios-android.png
           :width: 15 %
           :scale: 50 %

|

2. Hardware
===========

2.1. End-point Board
--------------------

Core of the entire system, main board connects to add-on Shields and Breakouts for additional functionality.

.. |ep1| image:: images/small/ep/EP-Top-Sideview.jpg
.. |ep2| image:: images/small/ep/EP-Top-Sideview-shielded.rev0.2.jpg
.. |ep3| image:: images/small/ep/bottomview.jpg
.. |ep4| image:: images/small/ep/topview.jpg

+---------+---------+-----------+-----------+
|  |ep1|  |  |ep2|  |   |ep3|   |   |ep4|   |
+---------+---------+-----------+-----------+

|

2.1.1. Block Diagram
********************

.. image:: images/IoT_EP_Board_Block-Diagram.jpg
    :align: center
    :alt: IoT EP Board Block Diagram


2.1.2. Product Specifications
*****************************


* CPU

  * ARM Cortex M4
  * 40MHz
  * DSP instruction and floating-point unit for efficient signal processing
  * 256 kB flash program memory


* Storage

  * 2 MB Flash


* RF Connectivity

  * Dual-band Radio

    * 2.4GHz: ZigBee, Thread, Bluetooth LE
    * Sub-GHz 915MHz (902MHz-930MHz), capability for 169MHz, 315MHz, 433MHz, 490MHz, 868MHz, 915MHz bands

  * Wake on Radio with signal strength detection, preamble pattern detection, frame detection and timeout
  * Modulation Formats

    * 2-FSK / 4-FSK with fully configurable shaping
    * Shaped OQPSK / (G)MSK
    * Configurable DSSS and FEC
    * BPSK / DBPSK TX
    * OOK / ASK


* Onboard Components

  * LSM303 - 3 axis Accelerometer & 3 axis Magnetometer
  * 2x Chip Antenna's 2.4GHz and 915MHz
  * 2x UFL Connectors for External Antennas
  * BGS 12AL7-6 E6327- RF Switch on each band for Antenna Diversity
  * OPA2316- Dual precision Op-Amps
  * M25P10-AVMP6TG TR- 2MB SPI Flash
  * FT232RQ - USB to UART Virtual Com Port
  * MCP73871 - Battery Charger and Management Unit
  * TPS63001 - Buck Boost Convertor
  * TPS22910 - 2x Load Switches for power saving
  * SC16IS - I2C to UART/RS485 Bridge
  * TPL0102 - Dual Independent Digital Potentiometer
  * 1 x User Debug-LED 1mA, software enable/disable for power save


* Power Consumption

  * Four (4) sleep modes S0 - S3
  * Current consumption during Sleep mode S3 less than 100uA
  * Current consumption on fully-active operation

    * At 2.4GHz TX/RX 21mA
    * At 915MHz TX/RX 41mA

  * Example battery life calculation with a 1000mAh battery

    * Battery Life with 2.4GHz Radio (2.5sec/hr full TX/RX) is approx. 2.6 Years
    * Battery Life with 915MHz Radio (2.5sec/hr full TX/RX) is approx. 2 Years


* Battery Management

  * Designed for Li-Polymer battery charge management
  * Capability for autonomous power source selection between input and battery
  * Simultaneously Powers the System and Charges the Battery.
  * Firmware status update for low-battery, power-good and charge status
  * Temperature range of -40°C to +85°C
  * Battery load maximum 800mA


* Expansion Connector

  * 40 PIN Board to Board Connector
  * Analog and Digital IO's
  * ADC 12bit 1Msps with 6 Input Channels
  * 1x Differential Analog Input 
  * 1x Analog Input with dual Stage High precision amplifier (Each Amplifier's gain is digitally Controllable)
  * 2x Analog Comparators
  * Controllable 4 Bit Current DAC with range .05uA to 64uA (Current Sink & Source Capability)
  * 2x 16Bit Timer/Counter
  * 1x 32Bit real time counter & Calendar
  * 1x 16Bit Low Energy Counter
  * 1x 32Bit Ultra Low Energy Counter/Timer
  * 1x 16Bit Pulse counter with Asynchronous Operation
  * 7x Compare/Capture/PWM Channels
  * 12x GPIO with Drive Strength 10mA
  * USB Virtual COM Port
  * Max 2 x UARTs, 1 x SPI, 1 x I2C, 1 x I2S, 1 x RS-485, 1 x IRDA, 1 x Smart Card


* Cryptography

  * Encryption/Decryption using 128bit Key 
  * Supports Autonomous Cipher Block modes (ECB, CTR, CBC, PCBC, CFB, CBC-MAC, GMAC, CCM, CCM* and GCM)
  * Accelerated SHA-1, SHA-224 and SHA-256
  * Accelerated Elliptic Curve Cryptography (ECC)
  * Legacy Algorithms: DES, MD4, MD5 and RC4
  * Implements all major Cryptographic Algorithms: AES, SHA-1, SHA-2  and ECC


* Board Power Supply Options

  * USB 5.0 V
  * 3.3V @800mA Output From Board to Board Connector
  * 5V input from Board to Board Connector


* Dimensions: 38mm x 49mm


* Software Development Tools

  * mbedOS SDK – C/C++ and Python
  * Industry standard opensource development tools
  * C and Python APIs


`Download Datasheet </files/IoT-EP-Board_Datasheet-rev1.1.pdf>`_

|

`Download Detailed Specifications document </files/EP_Board_Specs-rev1.1.pdf>`_

|

`Download PIN Map document </files/IoT-EP-Board_PinMap-rev1.1.pdf>`_

|

2.2. Program and Debug Board
----------------------------

Program and Debug shield for the main EP board. Connects to your host computer to target EP Board through USB to provide 
USB Disk drag and drop programming interface. It also provides a USB serial port that appears on a Linux machine as a tty interface.

.. |pdb1| image:: images/small/pdb/sideview1.jpg
.. |pdb2| image:: images/small/pdb/topview.jpg
.. |pdb3| image:: images/small/pdb/bottomview.jpg

+---------+---------+-----------+
|  |pdb1| |  |pdb2| |   |pdb3|  |
+---------+---------+-----------+

|

2.2.1. Product Specifications
*****************************

* USB Debug and Programming Adapter

* CMSIS-DAP Industrial Standard

* Serial Wire (SWD) Program and Debug Interface

* Drag & Drop MSD Flash Programming

* Virtual USB to Serial Port

|

2.3. Heart Rate Pulse Oximeter Board
------------------------------------

Sensor shield for monitoring and detecting pulse oximetry and heart-rate signals. Perfect for Fitness, Medical Monitoring, and Wearable Devices.

.. |hpo1| image:: images/small/hpo/sideview1.jpg
.. |hpo2| image:: images/small/hpo/topview.jpg
.. |hpo3| image:: images/small/hpo/bottomview.jpg

+---------+---------+-----------+
|  |hpo1| |  |hpo2| |   |hpo3|  |
+---------+---------+-----------+

|

2.3.1. Product Specifications
*****************************

* Max 30100 - I2C Pulse rate and Pulse oximeter Sensor with Programmable Sample rate

* Dimensions: 38mm x 30mm

|

2.4. IoT Gateway Board - C.H.I.P Breakout
-----------------------------------------

IoT Gateway board that connects EP Board to the `C.H.I.P Linux computer <https://getchip.com/pages/chip>`_.

.. |gw1| image:: images/small/gateway/gw_sideview.jpg
.. |gw2| image:: images/small/gateway/gw_topview.jpg
.. |gw3| image:: images/small/gateway/gw_bottomview.jpg

+---------+---------+-----------+
|  |gw1|  |  |gw2|  |   |gw3|   |
+---------+---------+-----------+

|

2.4.1. Product Specifications
*****************************

* Connectors compatible with C.H.I.P Linux Computer Headers

* Connectivity via SPI, UART and 8Bit Parallel DIO

* Dimensions: 40mm x 60mm

|

2.5. Navigation Board 
----------------------

IoT Navigation board is perfect high end navigation system for your quadcopters or any other system which requires critical navigation. 
This little Navigation board have IMU, Barometer, Motion processing unit, GPS and isolated four PWM digital Outputs.
On board motion processing unit fuses the IMU data and provides output in multiple formats like rotation matrix, quaternion, Euler Angle.
This motion processing unit reduces all the signal processing computation load from your target MCU.
This Navigation board have on board GPS + Patch Antenna and it generates standard NMEA sentence on UART while IMU communicates via SPI.
As this navigation board is a shield designed around IoT EP board which contains ARM Cortex M4 with DSP and floating point unit, 
ARM M4 is perfect for fusing the data of GPS and IMU data(processed) via extended kalman filter. 

Additional 4 optically isolated PWM's are provided specifically for controlling ESC for Quadcopter motors and also they can be utilized 
to control H Bridge for other vehicle control. 

With all these capabilities and ultra small form factor(38mm x 30mm) makes this board unique in market.

.. |nav1| image:: images/small/nav/nav_sideview.jpg
.. |nav2| image:: images/small/nav/nav_topview.jpg
.. |nav3| image:: images/small/nav/nav_bottomview.jpg

+---------+---------+-----------+
|  |nav1| |  |nav2| |   |nav3|  |
+---------+---------+-----------+

|

2.5.1. Product Specifications
*****************************

* Board Specifications

  * FormFactor: 38mm x 30mm
  * On board Sensors 3-axis Accelerometer, 3-axis Gyroscope, 3-axis Magnetometer and Barometer, GPS, Temperature and Humidity Sensors.
  * User programmable Digital Filters
  * IMU Self test and run time calibration Firmware
  * On board Motion processing unit to fuse the sensor data.
  * Multiple Output formats of motion processing unit: rotation matrix, quaternion, Euler Angles
  * 4 PWM optically isolated for ESC's


* Sensors Specifications

    * Accelerometer

      * Triple-axis accelerometer with a programmable full scale range of ±2g, ±4g, ±8g and ±16g and integrated 16-bit ADCs

    * Gyroscope
    
      * X-, Y-, and Z-Axis angular rate sensors (gyroscopes) with a user-programmable full scale range of ±250, ±500, ±1000, and ±2000°/sec and integrated 16-bit ADCs

    * Magnetometer 

      * 3-axis silicon monolithic Hall-effect magnetic sensor with magnetic concentrator with full scale measurement range is ±4800µT

    * Barometer 

      * Range 300hPa - 1100hPa with accuracy ±0.12Pa and resolution .2Pa or 1.7cm

    * Humidity

      * Range 0%RH - 100%RH with accuracy ±3RH and resolution 0.008%RH
    
    * Temperature

      * Range -40°C - 85°C with reosution 0.01°C
    
    * GPS
    
      * -165 dBm sensitivity, 10 Hz updates, 66 channels

    * Digital Motion Processing (DMP) Engine
      
      * Provides advanced Motion Processing and low power functions such as gesture recognition using programmable interrupts
      * Low-power pedometer functionality allows the host processor to sleep while the DMP maintains the step count

|

2.6. Optical and Thermal Vision Sensor
--------------------------------------

Optical and Thermal vision shield is a visual surveillance sensor with a dual-spectrum visible and Long-wave Infrared (Thermal) vision capabilities. 
Equipped with 1.3 Megapixel CMOS imager, Long-wave IR Flir Lepton thermal module, powerful MCU with outputs for Azimuth and Elevation servo control, and a microSD slot for on-board storage.

On-board MCU is to offload image and video signal processing from the target MCU on IoT EP Board or any other main board. 

.. |otv1| image:: images/small/otv/otv_sideview.jpg
.. |otv2| image:: images/small/otv/otv_topview.jpg
.. |otv3| image:: images/small/otv/otv_bottomview.jpg

+---------+---------+-----------+
|  |otv1| |  |otv2| |   |otv3|  |
+---------+---------+-----------+

|

2.6.1. Product Specifications
*****************************

* Board Specifications

  * FormFactor: 38mm x 30mm
  * Long-wave IR Thermal Module: `FLIR Lepton <http://www.flir.com/cores/lepton/>`_
  * Visible Spectrum CMOS Camera Chip: Omnivision OV9655
  * Feature-rich 0.25 inch, 1.3 megapixel CameraChip with LCD scaler and enhanced image processing
  * Power supervisory circuit for under and overvoltage lockout
  * Digital outputs for Azimuth servo, motorized zoom lens and Elevation servo control
  * Communication protocol: SPI

|

2.7. Board-to-board Flex Connector
----------------------------------

B2B Flex Connextor connects Breakouts and Shields to the EP Board via 40pin on-board connector. Flex connector allows for embedding
the boards in a variety of enclosures.

.. |b2b1| image:: images/small/b2b/sideview.jpg
.. |b2b2| image:: images/small/b2b/topview.jpg

+---------+---------+
| |b2b1|  |  |b2b2| |
+---------+---------+

|

2.7.1. Product Specifications
*****************************

* Flat Flex cable with small height board to board 40 Positions connector on both ends

* Length: 4cm

