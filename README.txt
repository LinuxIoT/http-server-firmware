PyWebServer

Author: Weqaar Janjua <weqaar.janjua@linuxiot.ie>


Light weight micro webserver written in Python, runs on top of Py Bottle



Usage:

Modify "LinuxIoTHttpServer.py" and add files to "images/" and "html/"

Run with "./start.sh"



Security:

Look elsewhere or use Apache/Nginx if you have security requirements. This is NOT a secure web server!

Use iptables/netfilter for filtering below the application layer.

