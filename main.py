#!/usr/bin/python

__author__ = "Weqaar Janjua"
__copyright__ = "Copyright (C) 2016 LinuxIoT.org"
__revision__ = "$Id$"
__version__ = "2.7"

import signal
import threading
import os
import sys
from time import *
import psutil
import subprocess
import gc
from sysconfigx import *
from LinuxIoTHttpServer import *


#Please modify the directory path (_firmware_dir) for the variable below: 
_firmware_dir = '.'
_thread_pool = []


class sysinit():
	def __init__(self):
		global sys_obj
		pass

	def run(self):
		sys_obj = Parser_Functions()
		sys_obj.parser_init()
		sys_obj.conf_map = sys_obj.ConfigSectionMap()
		self.verify_os_linux()
		#self.verify_run_as_root()
		self.set_working_dir()
		#self.set_process_priority_RT()

	def verify_os_linux(self):
		if "linux" in sys.platform:
			pass
		else:
			print "Firmware must run on Linux OS, other OS i.e. Windows are not supported, exiting!\n"
			exit (0)
		 
	def verify_run_as_root(self):
		if os.geteuid() != 0:
			print "Process must run as user root, exiting!\n"
			exit (0)
		 

	def set_working_dir(self):
		try:
			print "Switching to working directory: " + _firmware_dir + "\n"
			os.chdir(_firmware_dir)
		except OSError:
			print "ERROR Switching to working directory: " + _firmware_dir + " Exiting.\n"
			exit(0)

	def set_process_priority_RT(self):
		_pid = os.getpid()
		print "Process ID: " + str(_pid) + "\n"
		_process = psutil.Process(_pid)
		print "Current Process Priority: " + str(_process.nice()) + "\n"
		_process.nice(-20)
		print "New Process Priority: " + str(_process.nice()) + "\n"
		print "RT PRIO for " + subprocess.check_output(['chrt', '-p', str(_pid)]) + "\n"
		subprocess.call(['chrt', '-f', '-p', '99', str(_pid)])
		print "New RT PRIO for " + subprocess.check_output(['chrt', '-p', str(_pid)]) + "\n"



class LinuxIoT_Http_Server_Thread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)

	def run(self):
		_HTTP_SERVER = LinuxIoT_http_server()
		_HTTP_SERVER.start_server()


def main():
	_print_header()

	sysinit_obj = sysinit()
	sysinit_obj.run()

	_config_parse = Parser_Functions()
     	_config_parse.parser_init()
     	_config_parse.ConfigSectionMap()
	print "Garbage Collector Enabled: " + str(gc.isenabled()) + "\n"

    	# Spawn Threads
	t0 = LinuxIoT_Http_Server_Thread()
	t0.start()

	_thread_pool.append(t0)


def _print_header():
	_marker = '-------------------------------------------'
        _n = '\n'
	print _n + _marker
	print "Process name:" + __file__ + _n
	print "Author: " + __author__ + _n 
	print "Copyright: " + __copyright__ + _n
	print "Version: " + __version__ + _n
	print _marker + _n
	return 

if __name__ == '__main__':
	main()

