__author__ = "Weqaar Janjua"
__copyright__ = "Copyright (C) 2016 LinuxIoT.org"

from sysconfigx import *
from bottle import get, post, request, Bottle, run, error, static_file, response, template
import platform
import netifaces
import psutil
import datetime
import os
from uptime import uptime

class LinuxIoT_http_server():

	global app
	app = Bottle()

        def __init__(self):
		global _config
		global _http_ip
		global _http_port
		global _username
		global _passwd
	        _config = Parser_Functions()
                _config.parser_init()
                _config.ConfigSectionMap()
                _section = 'HTTP'
                _http_ip = _config.getSectionOption(_section, 'http_ip')
                _http_port = _config.getSectionOption(_section, 'http_port')

	@app.error(404)
	def error404(error):
		return static_file("404.html", root='html/')

	@app.route('/files/<filename:path>')
	def download(filename):
    		return static_file(filename, root='files/')

	@app.route('/images/<filename:path>')
	def download(filename):
    		return static_file(filename, root='images/', download=filename)

	@app.route('/docs/<filename:path>')
	def download(filename):
    		return static_file(filename, root='product-docs/build/')

	@app.route('/css/<filename:path>')
	def download(filename):
    		return static_file(filename, root='css/', download=filename)

	@app.route('/js/<filename:path>')
	def download(filename):
    		return static_file(filename, root='js/')

	@app.route('/html/<filename:path>')
	def download(filename):
		response.set_header('Content-Language', 'en')
    		return static_file(filename, root='html/')

	@app.route('/systeminformation')
	def sysinfopy():
		_html = """
                        <!DOCTYPE html>
                        <html>
                            <head>
                            <meta content="text/html; charset=windows-1252" http-equiv="content-type">
                            <link rel="stylesheet" type="text/css" href="/css/TextShadowHeading.css">
                            <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> 
                            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                            <script>
                                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                                ga('create', 'UA-82859282-1', 'auto');
                                ga('send', 'pageview');
                            </script>
                            <style>
                                body {
                                    width: 99%;
                                    height: 99%;
                                    background-color: #FFFFFF;
                                    font-family: 'Lato', sans-serif;
                                    font-size: 16px;
                                }
                            </style>
                            </head>
                            <body>
                                <br>
                                <center>
                                <a href="/"><img src="/images/linuxiot-logo-small-75p.png"></a>
                                <br>
                                <br>
                                <div class="heading-css">SYSTEM &amp; INFORMATION</div>
                                <br>
                                <br>
    					<table  style="width: 662px; height: 257px;"  border="0">
      					<tbody>
        					<tr>
          						<td style="text-align: left; background-color: rgb(192, 192, 192);"><span
               style="font-weight: bold;">Board & Processor Architecture
          						</td>
                                                        <td  style="text-align: center;">""" + "<a href='https://getchip.com/pages/chip'>C.H.I.P Computer</a><br>ARM Cortex A-8, V7-A" + "<br><img src='/images/webserverhw-15p.jpg'>" + """
          						</td>
        					</tr>
        					<tr>
          						<td style="text-align: left; background-color: rgb(192, 192, 192);"><span
               style="font-weight: bold;">Operating System
          						</td>
          						<td  style="text-align: center;">""" + str(platform.system()) + """
          						</td>
        					</tr>
        					<tr>
          						<td style="text-align: left; background-color: rgb(192, 192, 192);"><span
               style="font-weight: bold;">Kernel Version
          						</td>
          						<td  style="text-align: center;">""" + str(platform.uname()) + """
          						</td>
        					</tr>
        					<tr>
          						<td style="text-align: left; background-color: rgb(192, 192, 192);"><span
               style="font-weight: bold;">Uptime
          						</td>
          						<td  style="text-align: center;">""" + str(datetime.timedelta(seconds=uptime())) + """
          						</td>
        					</tr>
        					<tr>
          						<td style="text-align: left; background-color: rgb(192, 192, 192);"><span
               style="font-weight: bold;">Python Version
          						</td>
          						<td  style="text-align: center;">""" + str(platform.python_version()) + """
          						</td>
        					</tr>
        					<tr>
          						<td style="text-align: left; background-color: rgb(192, 192, 192);"><span
               style="font-weight: bold;">Webserver Middleware
          						</td>
                                                        <td  style="text-align: center;">""" + "<a href='https://bitbucket.org/LinuxIoT/http-server-firmware/wiki/Home'>http-server-firmware</a><br>runs on <a href='https://bottlepy.org/docs/dev/'>Python Bottle</a>" + """
          						</td>
        					</tr>
        					<tr>
          						<td style="text-align: left; background-color: rgb(192, 192, 192);"><span
               style="font-weight: bold;">Network Interfaces
          						</td>
          						<td  style="text-align: center;">""" + str(netifaces.interfaces()) + """
          						</td>
        					</tr>
      					</tbody>
    					</table>
					<br><br><hr  style="color: rgb(102, 102, 102); margin-left: auto; margin-right: auto; width: 50%;">LinuxIoT Gateway Service<br><br>&#169; LinuxIoT Project<br><br>
  				</center></body>
			</html>
		"""
		return _html

	@app.route('/')
	def main_page():
		response.set_header('Content-Language', 'en')
    		return static_file("index.html", root='html/')

	def start_server(self):
			run(app, host=_http_ip, port=_http_port, server='paste')
